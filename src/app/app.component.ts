import { Component } from "@angular/core";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.less"]
})
export class AppComponent {
  title = "Carbsy";
  response: any = null;

  constructor(private http: HttpClient) {

  }

  iate(): void {
    this.http.get("http://localhost:5000/api/hello").subscribe((data) => {
      this.response = data;
    });
    console.log("i ate, who? user, what? meal /snack");
  }
}
